import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import { Dashboard } from './components/Dashboard';
import 'primereact/resources/themes/saga-blue/theme.css';
import 'primereact/resources/primereact.min.css';
import './layout/flags/flags.css';
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.css';
import '@fullcalendar/core/main.css';
import '@fullcalendar/daygrid/main.css';
import '@fullcalendar/timegrid/main.css';
import './layout/layout.scss';
import './App.scss';

class App extends Component {

    render() {

        return (
            <div className="layout-wrapper layout-static  layout-static-sidebar-inactive">
                <div className="layout-main">
                    <Route path="/" exact component={Dashboard} />
                </div>
            </div>
        );
    }
}

export default App;
