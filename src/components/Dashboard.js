
import React, { Component } from 'react';
import { FullCalendar } from 'primereact/fullcalendar';
import { Card } from 'primereact/card';
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin from '@fullcalendar/interaction';
import timeGridPlugin from '@fullcalendar/timegrid';
import { Button } from 'primereact/button';
import Modal from './Modal';

const REACT_APP_BACKEND_URI = 'localhost:3000' ;

export class Dashboard extends Component {
   constructor(props) {
        super(props);
        const date = new Date();
        this.onClick = this.onClick.bind(this);
        this.state = {
            loading: true,
            show: false,
            room: null,
            roomsOp: [],
            objEvent: null,
            events: [],
            allDay: true,
            options: {
                plugins: [dayGridPlugin, timeGridPlugin, interactionPlugin],
                //timeZone: 'America/New_York',
                 defaultView: 'dayGridMonth',
                defaultDate: date,
                header: {
                    left: 'prev,next',
                    center: 'title',
                    right: 'toDay'
                },
                eventClick: (e) =>  {
                    const objEvent = { id:e.event.id, start: e.event.start, end: e.event.end, roomId: e.event.extendedProps.description }
                    this.setState({ show: true, objEvent })
                }
            }
        };
    }
    

    componentDidMount() {
        const httpProtocol = window.location.protocol === 'https:' ? 'https://' : 'http://'
        const httpUri = REACT_APP_BACKEND_URI ? `${httpProtocol}${REACT_APP_BACKEND_URI}` : '';
      
        return fetch(`${httpUri}/api/room/availability`, {
          method: 'GET',
          headers : { 
            'Content-Type': 'application/json',
          }
        }).then((response) => response.json())
        .then((data) => {

            this.setState({ events: data.response, loading: false })
            return fetch(`${httpUri}/api/rooms`, {
              method: 'GET',
              headers : { 
                'Content-Type': 'application/json',
              }
            }).then((response) => response.json())
            .then((data) => {
                const roomsOp = data.response.map(d => ({ value: d.id, label: d.name}))
                this.setState({ roomsOp })
            });                
        })
               
    }

    onClick(value) {
        const date = new Date();
        const objEvent = { id: null, start: date, end: date, roomId: value }
        this.setState({ show: true, objEvent })

    }

    render() {
        return (
            <div className="p-grid p-justify-center p-fluid dashboard">
                <div className="p-col-4">
                    <Card>
                        <div className="p-grid p-justify-between ">
                            <div className="p-col-6">
                                <h3>Rooms</h3>
                            </div>
                            <div className="p-col-4">
                                <Button label="Reserve" icon="pi pi-calendar" onClick={()=> this.setState({ show: true })} autoFocus />                    
                            </div>
                        </div>                        
                        {   this.state.roomsOp.map(x => (
                            <p className="list-rooms">
                                <span>
                                    {x.label}
                                </span>
                                <Button onClick={()=> this.onClick(x.value)}  icon="pi pi-search" className="p-button-rounded p-button-success p-button-text" />
                            </p>
                            ))
                        }
                    </Card>
                </div>
                <div className="p-col-8">
                    <Card>

                        <FullCalendar ref={(el) => this.fc = el} events={this.state.events} options={this.state.options}></FullCalendar>
                    </Card>
                </div>
                {   this.state.show &&                 
                    <Modal roomsOp = {this.state.roomsOp} events = {this.state.events} updateList={(list) => this.setState({ events: list })} event={this.state.objEvent} show={this.state.show} onHide={()=> this.setState({ show: false, objEvent: null })} />
                }
            </div>
        );
    }
}

Dashboard.propTypes = {
}

export default Dashboard;
