import React, { Component } from 'react';
import { Dropdown } from 'primereact/dropdown';
import { Calendar } from 'primereact/calendar';

export class Form extends Component {


    render() {
        return (
            <div className="p-grid" style={{margin:'10px 0px'}}>
				<div className="p-col-12">
                    <Dropdown
                        id="room"
                        scrollHeight="85px" 
                        value={this.props.type} 
                        options={this.props.roomsOp} 
                        onChange={(e) => this.props.onChangeType(e.value)} 
                        placeholder="None"
                    />
            	</div>
				<div className="p-col-12">
            		<div className="p-grid">
						<div className="p-col-6">
            				<Calendar 
                                minDate={this.props.minDate}
                                value={this.props.dateStart}
                                onChange={(e) => this.props.onChangeDateStart( e.value )} 
                                panelClassName="p-start"
                                showIcon 
                            />
            			</div>
						<div className="p-col-6">
            				<Calendar 
                                minDate={this.props.dateStart}
                                value={this.props.dateEnd}
                                onChange={(e) => this.props.onChangeDateEnd( e.value )} 
                                panelClassName="p-end"
                                showIcon 
                            />
            			</div>
            		</div>
            	</div>
            </div>
        );
    }
}

export default Form;
