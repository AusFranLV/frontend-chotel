import React, { Component } from 'react';
import { Dialog } from 'primereact/dialog';
import { Button } from 'primereact/button';
import { Messages } from 'primereact/messages';
import Form from './Form';
import moment from 'moment-timezone';

const REACT_APP_BACKEND_URI = 'localhost:3000' ;

export class Modal extends Component {
   constructor(props) {
        super(props);
        let today = new Date();
        let month = today.getMonth();
        let year = today.getFullYear();

        let minDate = new Date();
        minDate.setMonth(month);
        minDate.setFullYear(year);

        let type = null;
        let dateStart = today;
        let dateEnd = today;

        if (props.event) {
            type = props.event.roomId;
            dateStart = props.event.start;
            dateEnd = props.event.end ? props.event.end : props.event.start;
        }
        this.create = this.create.bind(this);
        this.update = this.update.bind(this);
        this.delete = this.delete.bind(this);
        this.search = this.search.bind(this);
        this.state = {
            change: true,
            roomsOp: [],
            minDate,
            type,
            dateStart,
            dateEnd,
        }
    }

    search() {
        this.clearMessages();
        const { type, dateStart, dateEnd } = this.state;
        let start = moment(dateStart).tz('America/New_York').format('YYYY-MM-DD');
        let end = moment(dateEnd).tz('America/New_York').format('YYYY-MM-DD');
        const httpProtocol = window.location.protocol === 'https:' ? 'https://' : 'http://'
        const httpUri = REACT_APP_BACKEND_URI ? `${httpProtocol}${REACT_APP_BACKEND_URI}` : '';
      
        return fetch(`${httpUri}/api/room/availability/${type}/${start}/${end}`, {
            method: 'GET',
            headers : { 
                'Content-Type': 'application/json',
            }
        }).then((response) => response.json())
        .then((data) => {
            if (data.statusCode === 200) {
                this.addMessagesInfo(data.mensaje)
                return;
            }
            if (data.statusCode === 400) {
                this.addMessagesError(data.mensaje)
                return;
            }
        })   
    }

    
    create() {
        this.clearMessages();
        const { type, dateStart, dateEnd } = this.state;
        let start = moment(dateStart).tz('America/New_York').format('YYYY-MM-DD');
        let end = moment(dateEnd).tz('America/New_York').format('YYYY-MM-DD');
        const httpProtocol = window.location.protocol === 'https:' ? 'https://' : 'http://'
        const httpUri = REACT_APP_BACKEND_URI ? `${httpProtocol}${REACT_APP_BACKEND_URI}` : '';
      
        return fetch(`${httpUri}/api/room/availability/${type}/${start}/${end}`, {
            method: 'POST',
            headers : { 
                'Content-Type': 'application/json',
            }
        }).then((response) => response.json())
        .then((data) => {
            if (data.statusCode === 200) {
                const list = [data.response,  ...this.props.events];
                this.props.updateList(list);
                this.props.onHide();
                return;
            }
            if (data.statusCode === 400) {
                this.addMessagesError(data.mensaje)
            }
        })   
    }


    update() {
        this.clearMessages();
        const { type, dateStart, dateEnd } = this.state;
        let start = moment(dateStart).tz('America/New_York').format('YYYY-MM-DD');
        let end = moment(dateEnd).tz('America/New_York').format('YYYY-MM-DD');
        const httpProtocol = window.location.protocol === 'https:' ? 'https://' : 'http://'
        const httpUri = REACT_APP_BACKEND_URI ? `${httpProtocol}${REACT_APP_BACKEND_URI}` : '';
      
        return fetch(`${httpUri}/api/room/availability/${this.props.event.id}/${type}/${start}/${end}`, {
            method: 'PUT',
            headers : { 
                'Content-Type': 'application/json',
            }
        }).then((response) => response.json())
        .then((data) => {
            if (data.statusCode === 200) {
                let list = this.props.events.filter(e =>  parseInt(e.id) !== parseInt(this.props.event.id));
                list = [data.response,  ...list];
                this.clearMessages();
                this.props.updateList(list);
                this.props.onHide();
                return;
            }

            if (data.statusCode === 400) {
                this.addMessagesError(data.mensaje)
            }
        })   
    }

    delete() {
        this.clearMessages();
        const httpProtocol = window.location.protocol === 'https:' ? 'https://' : 'http://'
        const httpUri = REACT_APP_BACKEND_URI ? `${httpProtocol}${REACT_APP_BACKEND_URI}` : '';
      
        return fetch(`${httpUri}/api/room/availability/${this.props.event.id}`, {
            method: 'DELETE',
            headers : { 
                'Content-Type': 'application/json',
            }
        }).then((response) => response.json())
        .then((data) => {
            
            if (data.statusCode === 200) {
                const list = this.props.events.filter(e => parseInt(e.id) !== parseInt(this.props.event.id));
                this.props.updateList(list);
                this.props.onHide();
            }
        })   
    }

    addMessagesError(menssage) {
        this.msgs2.show([
            { severity: 'error', detail: menssage, sticky: true }
        ]);
    }


    addMessagesInfo(menssage) {
        this.msgs2.show([
            { severity: 'success', detail: menssage, sticky: true }
        ]);
    }

    clearMessages() {
        if (this.msgs2) {
            this.msgs2.clear();
        }
    }

    render() {
        const header = this.props.event ? this.props.event.id ? 'Update booking' : 'Search room availability' : 'Add booking';
        return (
            <Dialog header={header} visible={this.props.show} style={{ width: '50vw' }}  onHide={this.props.onHide}>
                <Form 
                    minDate={this.state.minDate}
                    dateStart={this.state.dateStart} 
                    dateEnd={this.state.dateEnd} 
                    type={this.state.type} 
                    onChangeType={(type) => this.setState({ type })} 
                    onChangeDateStart={(dateStart) => this.setState({ dateStart })} 
                    onChangeDateEnd={(dateEnd) => this.setState({ dateEnd })} 
                    roomsOp={this.props.roomsOp} 
                />
                <Messages ref={(el) => this.msgs2 = el} />

                <div className="p-grid p-justify-end ">
                    {   this.props.event &&  this.props.event.id &&
                        <div className="p-col-2">
                            <Button label="delete" icon="pi pi-trash" onClick={this.delete} className="p-button-text " />
                        </div>
                    }
                    {   !this.props.event &&
                        <div className="p-col-2">
                            <Button label="Save" icon="pi pi-check" onClick={this.create} autoFocus />                    
                        </div>
                    }
                    {   this.props.event &&  this.props.event.id &&
                        <div className="p-col-2">
                            <Button label="Save" icon="pi pi-check" onClick={this.update} autoFocus />                    
                        </div>
                    }

                    {   this.props.event && !this.props.event.id &&
                        <div className="p-col-2">
                            <Button label="Search" icon="pi pi-search" onClick={this.search} autoFocus />                    
                        </div>
                    }
                </div>
            </Dialog>
        );
    }
}

export default Modal;