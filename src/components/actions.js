export const APP_UPDATE_FORM = 'APP_UPDATE_FORM';
export const APP_UPDATE_INPUT_FORM = 'APP_UPDATE_INPUT_FORM';
export const APP_TOGGLE_LOADING = 'APP_TOGGLE_LOADING';
export const APP_TOGGLE_ALERT = 'APP_TOGGLE_ALERT';
export const APP_TOGGLE_GROWL = 'APP_TOGGLE_GROWL';
export const APP_UPDATE_LIST = 'APP_UPDATE_LIST';

export const updateForm = (data) => ({
  type: APP_UPDATE_FORM,
  payload: {
    data,
  }
});

export const updateInputForm = (name, value) => ({
  type: APP_UPDATE_INPUT_FORM,
  payload: {
    name,
    value
  }
});

export const toggleLoading = (loading = false) => ({
  type: APP_TOGGLE_LOADING,
  payload: {
    loading,
  }
});

export const toggleAlert = (alert) => ({
  type: APP_TOGGLE_ALERT,
  payload: {
    alert
  }
});

export const toggleGrowl = (growl) => ({
  type: APP_TOGGLE_GROWL,
  payload: {
    growl
  }
});

export const updateList = (data) => ({
  type: APP_UPDATE_LIST,
  payload: {
    data,
  }
});

