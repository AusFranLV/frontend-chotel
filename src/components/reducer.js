import { Map } from 'immutable';
import moment from 'moment';

import { 
  APP_UPDATE_FORM, APP_UPDATE_INPUT_FORM, APP_TOGGLE_GROWL,
  APP_TOGGLE_LOADING, APP_TOGGLE_ALERT, APP_UPDATE_LIST,
} from './actions'

const today = new Date();
const formBooking = Map({
  type: null,
  dateStart: today,
  dateEnd:  today,
});

const defaultAlert = Map({
  show: false,
  message: null,
});

const appState = {
  list: [],
  loading: false,
  alert: defaultAlert,
  growl: null,
  form: formBooking,
  showModal: Map({
    show: false,
    type: null,
    dateStart: today,
    dateEnd:  today,
  }),
};

export default function callDetailRecorsReducer(state = Map(appState), action) {
  const { payload, type } = action;

  switch (type) {
    case APP_UPDATE_FORM:
      return state
        .setIn(['form'], payload.data);

    case APP_UPDATE_INPUT_FORM:
        return state.setIn(['form', payload.name], payload.value);
    case APP_TOGGLE_LOADING:
      return state.setIn(['loading'], payload.loading);
    case APP_TOGGLE_ALERT:
      return state.setIn(['alert'], payload.alert);
    case APP_TOGGLE_GROWL:
      return state.setIn(['growl'], payload.growl);
    case APP_UPDATE_LIST:
      return state.setIn(['list'], payload.data);

    default:
      return state;
  }
};
